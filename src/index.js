// https://bitbucket.org/sid-thobhani/ids-template-converter

// Include file system package
const file_system = require('fs');
// Include XML JS package
const xml_js_converter = require('xml-js');

// Input folder to load XML templates from
const input_folder = "src/templates_xml/";
// Input file extension
const input_file_ext = ".xml";

// Output folder path to export template JSON files
const output_folder = "src/templates_json/";
// Output file extension
const output_file_ext = ".studio";

// Loop through the templates folder in the input_folder folder
function readXMLFolder(inputFolder) {
	// Create base JSON out folder if it does not exist
	const jsonBaseFolder = output_folder;
	if (!file_system.existsSync(jsonBaseFolder)) {
		file_system.mkdirSync(jsonBaseFolder);
	}

	file_system.readdirSync(inputFolder).forEach(function (templateName) {
		// Get the XML file from the current template folder
		try {
			const xmlFile = file_system.readFileSync(inputFolder + templateName+ "/" + templateName + input_file_ext, "utf8");		
			createJsonDirAndFile(templateName, xmlFile)	;
		}
		catch (err) {
			console.log ("XML Error: ", err);
		}	
	});
}

// Create template folder in the JSON folder and JSON file 
function createJsonDirAndFile(templateName, templateXML) {
	try {
		// Create a folder with the templateName if it does not exist in the output_folder
		const outputDir = output_folder + templateName;
		if (!file_system.existsSync(outputDir)) {
			file_system.mkdirSync(outputDir);
		}

		// Create a folder named 'assets' if it does not exist in the template folder
		const templateAssetsFolder 

		// Create JSON file
		const jsonFile = xml_js_converter.xml2json(templateXML, {compact: true, spaces: 4});	
		pushJsonFileInOutputDir(outputDir, templateName, jsonFile);
	}
	catch (err) {
		console.log(err);
	}
}

// Push the JSON file in the output folder in the templateName sub-folder
function pushJsonFileInOutputDir(outputDir, templateName, templateJson) {
		file_system.writeFile(outputDir + "/" + templateName + output_file_ext, templateJson, function (err) {
		if (err) { 
			throw err;
		}
		else { 
			console.log('Template JSON saved!');
		}
	});
}

readXMLFolder(input_folder);